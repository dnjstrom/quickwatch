package se.aerivan.view;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Rect;
import android.util.Log;
import java.lang.Math;
import java.lang.NullPointerException;
import java.util.ArrayList;
import java.util.List;

public class ClockFace {

    private RectF box; //bounding box
    private int degFill;
    private Paint p;
    private int displayDetail;
    private int color;
    private int ticColor;
    private List<ClockFace> rings;
    private ClockFace ddBlock;


    public ClockFace() {
        p = new Paint();
		p.setAntiAlias(true);
        this.box = new RectF();
        setFill(360);
        displayDetail = 6;
        color = Color.BLACK;
        ticColor = Color.LTGRAY;
        rings = new ArrayList<ClockFace>();
    }

    public int getDisplayDetail() {
        return displayDetail;
    }

    public ClockFace setDisplayDetail(int detail) {
        displayDetail = detail;
        return this;
    }
    public ClockFace blockDisplayDetail(ClockFace face) {
        ddBlock = face;
        return this;
    }
    
    public int getFill() {
        return degFill;
    }

    public ClockFace addRing(ClockFace ring) {
        rings.add(ring);
        return this;
    }
    public boolean removeRing(ClockFace ring) {
        return rings.remove(ring);
    }
    public ClockFace clearRings() {
        rings.clear();
        return this;
    }

    public ClockFace setFill(int degrees) {
        if(degrees < 0) {
            degFill = 0;
        } else if (degrees > 360) {
            degFill = 360;
        } else {
            degFill = degrees;
        }

        return this; // For method chaining
    }


    public int getColor() {
        return color;
    }

    public ClockFace setColor(int color) {
        this.color = color;
        return this;
    }

    public int getTicColor() {
        return ticColor;
    }

    public ClockFace setTicColor(int color) {
        this.ticColor = color;
        return this;
    }

    public ClockFace set(int q, int x, int y, int z) {
        box.set(q,x,y,z);
        return this;
    }
    public ClockFace set(Rect box) {
        box.set(box);
        return this;
    }
    public ClockFace set(RectF box) {
        box.set(box);
        return this;
    }
    public ClockFace set(ClockFace cf) {
        this.box.set(cf.box);
        return this;
    }
    public ClockFace inset(float width, float height) {
        box.inset(width, height);
        return this;
    }
    public float centerX() {
        return box.centerX();
    }
    public float centerY() {
        return box.centerY();
    }
    public float top() {
        return box.top;
    }
    public float bottom() {
        return box.bottom;
    }
    public float left() {
        return box.left;
    }
    public float right() {
        return box.right;
    }
    public float height() {
        return box.height();
    }
    public float width() {
        return box.width();
    }

    public void draw(Canvas canvas) {
        p.setColor(color);

        canvas.drawArc(box, -90, degFill, true, p);

        if (degFill == 0) { // draw a line
            float x = box.centerX();
            canvas.drawLine(x, box.centerY(), x, box.top, p);
        } else { // draw an arc
            canvas.drawArc(box, -90, degFill, true, p);
            drawTics(canvas);
        }

        //TODO: make arc center see-though
    }

    private void drawTics(Canvas canvas) {
        //TODO: Make tic-coloring adapt when background is bright

        int brightest = Math.max(Color.red(ticColor), Math.max(Color.green(ticColor), Color.blue(ticColor)));

        float mod;
        if(brightest > 127) {
            mod = 0.6f;
        } else {
            mod = 2.5f;
        }

        if(displayDetail <= 6) {
            p.setColor(Color.rgb(
                        (int) (mod * Color.red(ticColor)),
                        (int) (mod * Color.green(ticColor)),
                        (int) (mod * Color.blue(ticColor))
                        ));
            drawTicsHelper(canvas, 6);
        }

        if(displayDetail <= 30) {
            p.setColor(ticColor);
            drawTicsHelper(canvas, 30);
        }


        if(ddBlock != null) {
            p.setColor(color);
            canvas.drawArc(ddBlock.box, -90, degFill, true, p);
        }

        if(displayDetail <= 90) {
            p.setColor(ticColor);
            drawTicsHelper(canvas, 90);
        }

        outline(canvas);
    }

    private void drawTicsHelper(Canvas canvas, int step) {
        float hyp = box.width() / 2;
        float startx = box.centerX();
        float starty = box.centerY();

        for( int deg = 0; deg < degFill; deg += step ) {
            float endx = startx + (float) (hyp * Math.cos(Math.toRadians(deg - 90)));
            float endy = starty + (float) (hyp * Math.sin(Math.toRadians(deg - 90)));

		    canvas.drawLine(startx, starty, endx, endy, p);
        }
    }

    private void outline(Canvas canvas) {
        p.setColor(ticColor);
        p.setStyle(Paint.Style.STROKE);
        for(ClockFace ring : rings) {
            canvas.drawArc(ring.box, -90, degFill, true, p);
        }
        p.setStyle(Paint.Style.FILL);
    }
}
