package se.aerivan.fragments;

import se.aerivan.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Stopwatch extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if(container == null) {
			return null;
		} else {
			return inflater.inflate(R.layout.stopwatch, container, false);
		}
	}
}
