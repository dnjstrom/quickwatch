package se.aerivan.main;

import java.util.ArrayList;
import java.util.List;

import se.aerivan.R;
import se.aerivan.fragments.Countdown;
import se.aerivan.fragments.Stopwatch;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;

public class MainActivity extends FragmentActivity {

	private PagerAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		initialisePaging();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void initialisePaging() {
		List<Fragment> fragments = new ArrayList<Fragment>();

		fragments.add(Fragment.instantiate(this, Countdown.class.getName()));
		fragments.add(Fragment.instantiate(this, Stopwatch.class.getName()));

		this.adapter = new MainPagerAdapter(super.getSupportFragmentManager(), fragments);
		ViewPager pager = (ViewPager) super.findViewById(R.id.viewpager);
		pager.setAdapter(adapter);
	}
}
