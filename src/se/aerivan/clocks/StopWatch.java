package se.aerivan.clocks;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

public class StopWatch extends BaseClock {

	private boolean running;

	private UpdateTask updateTask;

	private static final int REFRESH_RATE = 10;

	private GestureDetector detector;

	public StopWatch(Context context) {
		this(context, null, 0);
	}

	public StopWatch(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public StopWatch(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		running = false;
		detector = new GestureDetector(context, new GestureListener());
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return detector.onTouchEvent(event);
	}

	private void start() {
		running = true;

		updateTask = new UpdateTask();
		updateTask.execute();
	}

	private void stop() {
		updateTask.cancel(false);
		running = false;
	}

	private void reset() {
		if(running) {
			stop();
		}

		setDisplayTime(0);
		invalidate(clockFace);
	}

	private class GestureListener extends SimpleOnGestureListener {
		@Override
		public boolean onDown(MotionEvent ev) {
			super.onDown(ev);
			return true; //Mysteriously, this seems to be needed.
		}

		@Override
		public boolean onSingleTapUp(MotionEvent ev) {
			super.onSingleTapUp(ev);
			if (running) {
				stop();
			} else {
				start();
			}
			return true;
		}
		
		@Override
		public void onLongPress(MotionEvent ev) {
			super.onLongPress(ev);
			reset();
		}
	}

	private class UpdateTask extends AsyncTask<Void, Void, Void> {
		private long oldMillis;

		public UpdateTask() {
			oldMillis = SystemClock.elapsedRealtime();
		}

		@Override
		protected Void doInBackground(Void... params) {
			while (true) {
				long currentMillis = SystemClock.elapsedRealtime();
				modDisplayTime((int) (currentMillis - oldMillis) / 1000);
				oldMillis = currentMillis;

				publishProgress();

				if(isCancelled()) {
					break;
				}

				try {
					Thread.sleep(REFRESH_RATE);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... v) {
			invalidate(clockFace);
		}

		@Override
		protected void onCancelled(Void v) {
			updateTask = null;
		}
	}
}
