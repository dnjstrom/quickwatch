package se.aerivan.clocks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.SystemClock;
import android.widget.Toast;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import java.lang.Math;


public class CountdownClock extends BaseClock {

	private float velocity;
	private int direction = 0;
	private static final float DECELERATION = 9.81f;
	private long timeDone;
	private boolean running = false;
	private int fps = 40;

	private FlingTask flingTask;
	private TimerTask timerTask;

	private GestureDetector detector;
	private Point screenSize;

	private int currentDisplayTime;

	public CountdownClock(Context context) {
		this(context, null, 0);
	}

	public CountdownClock(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public CountdownClock(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		velocity = 0;
		detector = new GestureDetector(context, new GestureListener());

		WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();

		screenSize = new Point();
		display.getSize(screenSize);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return detector.onTouchEvent(event);
	}

	private void start() {
		running = true;
		timeDone = SystemClock.elapsedRealtime() + getDisplayTime() * 1000;

		timerTask = new TimerTask();
		timerTask.execute();
	}

	private void stop() {
		running = false;
		timerTask.cancel(false);
	}

	private void reset() {
		if(running) {
			stop();
		}
		setDisplayTime(0);
        invalidate();
	}

	private void notifyTimesUp() {
		Log.d("notifyTimesUp", "Time's up!");
		//getContext().runOnUiThread(new Runnable() {
			//@Override
			//public void run() {
				//Toast.makeText(getContext(), "Time's up!", Toast.LENGTH_LONG).show();
			//}
		//});
	}

	private class GestureListener extends SimpleOnGestureListener {
		@Override
		public boolean onDown(MotionEvent ev) {
			super.onDown(ev);
			if (flingTask != null) {
				flingTask.cancel(true);
			}
			currentDisplayTime = getDisplayTime();
			return true;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent ev) {
			super.onSingleTapConfirmed(ev);
			if(running) {
				stop();
			} else {
				start();
			}
			return true;
		}

		@Override
		public void onLongPress(MotionEvent ev) {
			super.onLongPress(ev);
			reset();
		}


		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			if(!running) {
				float factor = 1 - (e1.getX(0)/(float) screenSize.x);

				float start = e1.getY(0);
				float end = e2.getY(e2.getPointerCount() - 1);

				float scrollFactor = (start-end)/screenSize.y;
				//Log.d("CountDownClock", "" + scrollFactor);

				int timeStep;
				int steps = 60;
				if(factor > 0.7) {
					timeStep = 3600;
					steps = 10;
				} else if(factor < 0.3) {
					timeStep = 1;
				} else {
					timeStep = 60;
				}

				//Log.d("CountdownClock", "distanceY: " + distanceY + ", timeStep: " + timeStep);
				//modDisplayTime(Math.round(distanceY/5) * timeStep);
				setDisplayTime((int)(currentDisplayTime + ((int)(steps * scrollFactor)) * timeStep));

				CountdownClock.this.invalidate();
				return true;
			} else {
				return false;
			}
		}

		//@Override
		//public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			//if(!running) {
				//velocity = Math.abs(velocityY);
				//direction = (int) -Math.signum(velocityY);

				//if (flingTask != null) {
					//flingTask.cancel(true);
				//}

				//flingTask = new FlingTask();
				//flingTask.execute();

				//return true;
			//} else {
				//return false;
			//}
			//return false;
		//}
	}

	private class FlingTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			while (velocity > 10) {
				modDisplayTime(Math.round(direction * velocity));
				velocity = (float) Math.pow(velocity, 0.95);
				publishProgress();

				if(isCancelled()) {
					break;
				}

				try {
					Thread.sleep(1000/fps);
				} catch (InterruptedException e) {}
			}

			velocity = 0; // Make sure velocity is not left negative
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... v) {
            invalidate();
		}

		@Override
		protected void onCancelled(Void v) {
			velocity = 0;
			flingTask = null;
		}
	}

	private class TimerTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			while(running) {
				long currentMillis = SystemClock.elapsedRealtime();
				setDisplayTime((int) (timeDone - currentMillis) / 1000);

				if(getDisplayTime() == 0) {
					notifyTimesUp();
					reset();
					break;
				}

				publishProgress();

				if(isCancelled()) {
					break;
				}

				try {
					Thread.sleep(1000/fps);
				} catch (InterruptedException e) {}
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... v) {
            invalidate();
		}

		@Override
		protected void onCancelled(Void v) {
			timerTask = null;
		}
	}
}
