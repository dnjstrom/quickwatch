package se.aerivan.clocks;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.format.Time;
import android.util.AttributeSet;
import android.view.View;

import android.util.Log;
import java.lang.Math;
import se.aerivan.view.ClockFace;

public abstract class BaseClock extends View {

    private ClockFace clockFace;
    private ClockFace secondFace;
    private ClockFace minuteFace;
    private ClockFace hourFace;

	private Paint paint;
	private boolean changed;

	private int displayTime;
    private int maxDisplayTime;
    private boolean displayMaxTime;
	private float barWidth;


	public BaseClock(Context context) {
		this(context, null, 0);
	}

	public BaseClock(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public BaseClock(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		changed = true;
        maxDisplayTime = 18000; // 5 hours
        displayMaxTime = false;
		displayTime = 0;

		paint = new Paint();
		paint.setAntiAlias(true);

        clockFace = new ClockFace();
        secondFace = new ClockFace();
        secondFace.setColor(Color.WHITE);
        secondFace.setTicColor(Color.DKGRAY);

        minuteFace = new ClockFace();
        minuteFace.setColor(Color.GREEN);
        minuteFace.setTicColor(Color.DKGRAY);

        hourFace = new ClockFace();
        hourFace.setColor(Color.RED);
        hourFace.setTicColor(Color.DKGRAY);
        hourFace.setDisplayDetail(90);

        clockFace.addRing(minuteFace);
        clockFace.addRing(hourFace);
        clockFace.blockDisplayDetail(hourFace);

        secondFace.addRing(minuteFace);
        secondFace.addRing(hourFace);
        secondFace.blockDisplayDetail(hourFace);

        minuteFace.addRing(hourFace);
        minuteFace.blockDisplayDetail(hourFace);
	}

	public void setDisplayTime(int displayTime) {
        displayMaxTime = false;

		if(displayTime < 0) {
			this.displayTime = 0;
		} else if (displayTime >= maxDisplayTime) {
			this.displayTime = maxDisplayTime;
            displayMaxTime = true;
		} else {
			this.displayTime = displayTime;
		}
	}

	public int getDisplayTime() {
		return displayTime;
	}

	public void modDisplayTime(int mod) {
		setDisplayTime(getDisplayTime() + mod);
	}

	protected int sToD(int time) {
		return time * 6;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

		int radius = Math.min(w, h);
		radius *= 0.4; //0.8 / 2

		clockFace.set(w / 2 - radius, h / 2 - radius, w / 2 + radius, h / 2 + radius);

        secondFace.set(clockFace);

		barWidth = clockFace.height() / 6;

        minuteFace.set(secondFace).inset(barWidth, barWidth);

        hourFace.set(minuteFace).inset(barWidth, barWidth);

		changed = true;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		boolean changed = this.changed;
		if (changed) {
			this.changed = false;
		}

        clockFace.draw(canvas);

        if (displayMaxTime) { //TODO: Fix the last minute before 5 hours
            secondFace.setFill(360);
            secondFace.draw(canvas);
            minuteFace.setFill(360);
            minuteFace.draw(canvas);
            hourFace.setFill(360);
            hourFace.draw(canvas);
        } else {
            secondFace.setFill(sToD(displayTime % 60));
            secondFace.draw(canvas);
            minuteFace.setFill(sToD((displayTime % 3600) / 60));
            minuteFace.draw(canvas);
            hourFace.setFill(90 * ((displayTime % 86400) / 3600));
            hourFace.draw(canvas);
        }

        paint.setColor(clockFace.getColor());
        canvas.drawCircle(clockFace.centerX(), clockFace.centerY()
                , 20, paint);
	}

}
